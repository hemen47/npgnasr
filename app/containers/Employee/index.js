import React from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import './style.css';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { useInjectReducer } from 'utils/injectReducer';
import profile from './profile.png';
import { addToCompareList, removeFromCompareList } from './actions';
import { makeSelectCompareList } from './selectors';
import reducer from './reducer';

const key = 'employee';

function Employee({
  employee,
  callAddToCompareList,
  callRemoveFromCompareList,
  compareList,
}) {
  useInjectReducer({ key, reducer });

  const containsObject = (obj, list) => {
    let i;
    // eslint-disable-next-line no-plusplus
    for (i = 0; i < list.length; i++) {
      if (list[i] === obj) {
        return true;
      }
    }

    return false;
  };

  const [showButton, setShowButton] = React.useState(false);
  return (
    <div
      className="employee"
      onMouseEnter={() => setShowButton(true)}
      onMouseLeave={() =>
        containsObject(employee, compareList) ? null : setShowButton(false)
      }
    >
      <img src={profile} alt="profile" />
      {showButton && (
        <div className="button-container">
          {containsObject(employee, compareList) ? (
            // eslint-disable-next-line jsx-a11y/no-static-element-interactions,jsx-a11y/click-events-have-key-events
            <span
              className="button-remove"
              onClick={() => callRemoveFromCompareList(employee)}
            >
              REMOVE
            </span>
          ) : (
            // eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions
            <span
              className="button-add"
              onClick={() => callAddToCompareList(employee)}
            >
              COMPARE
            </span>
          )}
        </div>
      )}
      <div>
        <p>{employee.employee_name}</p>
        <p>Salary: {employee.employee_salary} $</p>
        <p>Age: {employee.employee_age}</p>
      </div>
    </div>
  );
}
Employee.propTypes = {
  employee: PropTypes.object,
  compareList: PropTypes.array,
  callAddToCompareList: PropTypes.func,
  callRemoveFromCompareList: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    callAddToCompareList: item => dispatch(addToCompareList(item)),
    callRemoveFromCompareList: item => dispatch(removeFromCompareList(item)),
  };
}

const mapStateToProps = createStructuredSelector({
  compareList: makeSelectCompareList(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
export default compose(withConnect)(Employee);
