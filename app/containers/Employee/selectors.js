/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectEmployees = state => state.employee || initialState;

export const makeSelectCompareList = () =>
  createSelector(
    selectEmployees,
    employees => employees.compareList,
  );
