import { ADD_TO_COMPARE_LIST, REMOVE_FROM_COMPARE_LIST } from './constants';

export function addToCompareList(item) {
  return {
    type: ADD_TO_COMPARE_LIST,
    payload: item,
  };
}
export function removeFromCompareList(item) {
  return {
    type: REMOVE_FROM_COMPARE_LIST,
    payload: item,
  };
}
