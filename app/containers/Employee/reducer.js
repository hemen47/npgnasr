import produce from 'immer';
import { ADD_TO_COMPARE_LIST, REMOVE_FROM_COMPARE_LIST } from './constants';

// The initial state of the App
export const initialState = {
  compareList: [],
};

/* eslint-disable default-case, no-param-reassign */
const reducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case ADD_TO_COMPARE_LIST:
        if (draft.compareList.length <= 3) {
          draft.compareList.push(action.payload);
        }
        break;

      case REMOVE_FROM_COMPARE_LIST:
        // eslint-disable-next-line no-case-declarations
        const index = draft.compareList.findIndex(
          item => item.id === action.payload.id,
        );
        if (index !== -1) draft.compareList.splice(index, 1);
        break;
    }
  });

export default reducer;
