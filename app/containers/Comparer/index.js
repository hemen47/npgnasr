import React from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { makeSelectCompareList } from '../Employee/selectors';
import './style.css';

function Comparer({ compareList }) {
  return (
    <div className="comparer">
      <p>You can select up to 4 employees in order to compare them!</p>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            {compareList.map((d, i) => (
              // eslint-disable-next-line react/no-array-index-key
              <th key={i}>{d.employee_name}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>Salary</th>
            {compareList.map((d, i) => (
              // eslint-disable-next-line react/no-array-index-key
              <td key={i}>{d.employee_salary} $</td>
            ))}
          </tr>
          <tr>
            <th>Age</th>
            {compareList.map((d, i) => (
              // eslint-disable-next-line react/no-array-index-key
              <td key={i}>{d.employee_age}</td>
            ))}
          </tr>
        </tbody>
      </table>
    </div>
  );
}

Comparer.propTypes = {
  compareList: PropTypes.array,
};

const mapStateToProps = createStructuredSelector({
  compareList: makeSelectCompareList(),
});

const withConnect = connect(mapStateToProps);
export default compose(withConnect)(Comparer);
