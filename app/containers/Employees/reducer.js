import produce from 'immer';
import { GET_DATA_SUCCESS, GET_DATA_ERROR } from './constants';

// The initial state of the App
export const initialState = {
  employees: [],
  error: null,
};

/* eslint-disable default-case, no-param-reassign */
const reducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_DATA_SUCCESS:
        draft.employees = action.payload;
        break;

      case GET_DATA_ERROR:
        draft.error = action.payload;
        break;
    }
  });

export default reducer;
