import { call, put, takeLatest } from 'redux-saga/effects';
import request from 'utils/request';

import { GET_DATA } from './constants';
import { getDataError, getDataSuccess } from './actions';

export function* getDataSaga() {
  const requestURL = `http://dummy.restapiexample.com/api/v1/employees`;

  try {
    const response = yield call(request, requestURL);
    yield put(getDataSuccess(response.data));
  } catch (err) {
    yield put(getDataError(err));
  }
}

export default function* githubData() {
  yield takeLatest(GET_DATA, getDataSaga);
}
