import { GET_DATA, GET_DATA_SUCCESS, GET_DATA_ERROR } from './constants';

export function getData() {
  return {
    type: GET_DATA,
  };
}
export function getDataSuccess(response) {
  return {
    type: GET_DATA_SUCCESS,
    payload: response,
  };
}
export function getDataError(error) {
  return {
    type: GET_DATA_ERROR,
    payload: error,
  };
}
