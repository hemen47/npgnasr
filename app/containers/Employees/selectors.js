/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectEmployees = state => state.employees || initialState;

export const makeSelectEmployees = () =>
  createSelector(
    selectEmployees,
    employees => employees.employees,
  );
export const makeSelectError = () =>
  createSelector(
    selectEmployees,
    employees => employees.error,
  );
