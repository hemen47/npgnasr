/*
 * Employees
 *
 */

import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import { makeSelectEmployees, makeSelectError } from './selectors';
import { getData } from './actions';
import reducer from './reducer';
import saga from './saga';
import Employee from '../Employee';
import './style.css';
import Comparer from '../Comparer';

const key = 'employees';

function Employees({ callGetData, employees, error }) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    callGetData();
  }, []);

  useEffect(() => {
    if (error) {
      console.log(error.response);
    }
  }, [error]);

  return (
    <div className="employees">
      <div className="employees-container">
        {employees.slice(0, 6).map((employee, i) => (
          // eslint-disable-next-line react/no-array-index-key
          <Employee key={i} employee={employee} />
        ))}
      </div>
      <Comparer />
    </div>
  );
}

Employees.propTypes = {
  callGetData: PropTypes.func.isRequired,
  employees: PropTypes.array.isRequired,
  error: PropTypes.object,
};

export function mapDispatchToProps(dispatch) {
  return {
    callGetData: () => dispatch(getData()),
  };
}

const mapStateToProps = createStructuredSelector({
  employees: makeSelectEmployees(),
  error: makeSelectError(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
export default compose(withConnect)(Employees);
